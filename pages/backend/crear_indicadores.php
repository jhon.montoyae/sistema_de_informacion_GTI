
<link rel="stylesheet" href="plugins/alertify.default.css">
<link rel="stylesheet" href="plugins/alertify.core.css">
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/alertify.min.js"></script>

<style>


table {  color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse;}

th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
    border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }

td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
    color: #669;    border-top: 1px solid transparent; }

tr:hover td { background: #d0dafd; color: #339; }


	.select2-container--default .select2-selection--single, .w3-input
	{
		padding: 5px;
	    display: block;
	    border: none;
	    border-bottom: 1px solid #ccc;
	}
	
	.ms-options ms-active
	{
		min-height: 100px; 
		max-height: 100px;
	}
	
	.ms-drop.bottom
	{
		width: 310px;
		
	}

    #busqueda{

    
    width: 204px;
    line-height: 18px;
}


#buscar{
    margin-left: 34px;
        padding: 7px;
}


#btnExport{
   cursor: pointer;
    width: 65px;
     margin-right: 594px;
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    padding: 0.5em 1em;
    border: 1px solid #999;
    border-radius: 2px;
    cursor: pointer;
    font-size: 0.88em;
    color: black;
    white-space: nowrap;
    overflow: hidden;
    background-color: #e9e9e9;
    background-image: -webkit-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -moz-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -ms-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -o-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: linear-gradient(to bottom, #fff 0%, #e2dcdc 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#e9e9e9');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;

    
}


</style>



<?php
session_start ();
if ($_SESSION ['authenticated'] == 1) {
	include("../../modelo/conexion.php");
    $con=new conexion();
    
	
	//Obtiene el valor del radioboton que determina la manera de buesqueda del indicador
	$busqueda=$_POST['rbtnBusqueda'];
	
	$mes=$_POST['mes'];
	
	
	$nombre_mes=$con->conexion->query("select descripcion from mes where id_mes=$mes ");
	
	$mes_nom=$nombre_mes->fetch_assoc();
	
	if ($busqueda=='general'){
		
		$res=$con->conexion->query("SELECT a.nombre,b.cliente,b.zona,c.ANS from servicio_indicador a,
       cliente b, cliente_servicio c where c.id_cliente=b.id_cliente and c.id_servicio=a.id_servicio");
		
	}else{
		$cliente=$_POST['clientes'];
		
		$res=$con->conexion->query("SELECT a.nombre,b.cliente,b.zona,c.ANS from servicio_indicador a, cliente b, 
       cliente_servicio c where c.id_cliente=b.id_cliente and c.id_servicio=a.id_servicio and b.id_cliente=$cliente");
	}
	

}
?>
 
 <div style=" width: 101.5%; height:280px; overflow-y: scroll;">


<table id="indicadores_operacion">  
         
          <tr id="tbl_titulo">
           
           <td><b>DESCRIPCIÓN NIVEL SERVICIO </b></td>
           <td><b>CLIENTE</b></td>
           <td><b>ZONA</b></td>
           <td><b>FECHA</b></td>
           <td><b>ANS ACORDADO </b></td>
           <td><b>INDICADOR CR (%)</b></td>
           <td><b>INDICADOR CL (%)</b></td>
           <td><b>INDICADOR GRAL (%)</b></td>
           <td><b>CUMPLIMIENTO (%)</b></td>
           <td><b>JUSTIFICACIÓN</b></td>
           <td><b>PLAN DE ACCIÓN</b></td>
          
          </tr>
      
        
       <tbody>
     <?php $i=1; while($row=$res->fetch_assoc()){ ?>
  
         <tr>     	
           <td><label id="nombre_servicio<?php echo $i;?>" ><?php  echo $row['nombre'] ?></label></td>
           <td><label id="cliente<?php echo $i;?>"  ><?php  echo $row['cliente'] ?></label></td>
           <td><label id="zona<?php echo $i;?>" ><?php  echo $row['zona'] ?></label></td>
           <td><label id="mes<?php echo $i;?>" ><?php  echo $mes_nom['descripcion']." ". date ("Y");  ?></label></td>
           <td><input id="ans<?php echo $i;?>" name="ans" value="<?php  echo $row['ANS'] ?>" class="w3-input war" type="text" style="width:75px" readonly></td>
           <td><input id="ind_cr<?php echo $i;?>" name="ind_cr"  class="w3-input war" type="number" style="width:75px" required></td>
           <td><input id="ind_cl<?php echo $i;?>" name="ind_cl"  class="w3-input war" type="number" style="width:75px" required></td>
           <td><input onmouseover="calculo(<?php echo $i;?>)" id="ind_gral<?php echo $i;?>" name="ind_gral"  class="w3-input war" type="text" style="width:75px"  readonly></td>
           <td><input id="cumplimiento<?php echo $i;?>" name="cumplimiento"  class="w3-input war" type="text" style="width:75px" readonly></td>
           <td><input id="justificacion<?php echo $i;?>" name="justificacion"  class="w3-input war" type="text" style="width:75px" ></td>
           <td><input id="plan_acc<?php echo $i;?>" name="plan_acc"  class="w3-input war" type="text" style="width:75px" ></td>
             </tr> 	
      <?php $i++;}
             
             
             
             ?>
           </tbody>
               <div id="excel"> 
  <!--  <button id="btnExport" class="btn btn-danger pull-right">Excel</button>    -->       
 
     
  <label id="buscar"><b>Buscar:<b></label>
  <input type="text" id="busqueda">

          </div>  
           
                  
        </table>

   </div>
   
<script>
function calculo(valor){
	num=valor;
	
	var x = document.getElementById('ind_cr'+num).value;
	var y = document.getElementById('ind_cl'+num).value;
	var z= document.getElementById('ans'+num).value;

    patron = "%";
    nuevoValor    = "";
    nuevaCadena = z.replace(patron, nuevoValor);
   
    patron1=",",
    nuevoValor1=".",
    nuevaCadena1=nuevaCadena.replace(patron1,nuevoValor1);
    
    //var b = parseFloat(nuevaCadena);
   
    
	var res = (x/100)*(y/100);
	var div=(x/100)/(nuevaCadena1/100);

	
    var general=res*100;
    var cumpli=div*100;


    var gen = general.toFixed(2);
    var cum = cumpli.toFixed(2);


      $('#ind_gral'+num).val(gen);
      $('#cumplimiento'+num).val(cum);
    
	//$('#ind_gral'+num).val(res*100); 
	//$('#cumplimiento'+num).val(div_n*100); 
	//$('#ind_gral'+num).val(res/100+"%"); //pone el resultado en el input
	//$('#cumplimiento'+num).val(div*100); //pone el resultado en el input


}
</script>


<script>
function inserta_datos(){
  
	var num_rows=document.getElementById("indicadores_operacion").rows.length;
     
	
	
	//var num_cols=11;
 // document.write(num_rows);
	for(i=1;i<=num_rows;i++){
		
		 //document.write(num_rows);
    	var servicio = document.getElementById('nombre_servicio'+i).innerHTML;	    	
    	var cliente = document.getElementById('cliente'+i).innerHTML;
    	var zona = document.getElementById('zona'+i).innerHTML;
    	var mes = document.getElementById('mes'+i).innerHTML;
    	var ans = document.getElementById('ans'+i).value;    
    	var ind_cr = document.getElementById('ind_cr'+i).value;
    	var ind_cl = document.getElementById('ind_cl'+i).value;
    	var ind_gral = document.getElementById('ind_gral'+i).value;    	
    	var cumplimiento = document.getElementById('cumplimiento'+i).value;
    	var justificacion = document.getElementById('justificacion'+i).value;
    	var plan_acc = document.getElementById('plan_acc'+i).value;
    	var id_mes  = '<?php echo $mes; ?>';

    

    	   realizaProceso(servicio,cliente,zona,mes,ans,ind_cr,ind_cl,ind_gral,cumplimiento,
    			   justificacion,plan_acc,id_mes);
        
    	    	}

}



function realizaProceso(servicio,cliente,zona,mes,ans,ind_cr,ind_cl,ind_gral,cumplimiento,
		   justificacion,plan_acc,id_mes) {
	
    var parametros = {
        "servicio": servicio,     
        "cliente": cliente,
        "zona": zona,
        "mes" : mes,
        "ans": ans,
        "ind_cr": ind_cr,
        "ind_cl": ind_cl,
        "ind_gral":ind_gral,
        "cumplimiento": cumplimiento,
        "justificacion": justificacion,
        "plan_acc":plan_acc,
        "id_mes": id_mes
    };
    $.ajax({
        data: parametros,
        url: 'pages/backend/includes/insertar_indicadores.php',
        type: 'post',
       
        success: function (response) {
            $("#resultado").html(response);
        	window.setTimeout('location.reload()');
        }
    });
}


/*$("#btnExport").click(function(){
	  $("#indicadores_operacion").table2excel({
	    filename: "Indicadores de Operación"
	  }); 
	});*/

</script>


<!-- script para filtrar en la busqueda de la tabla -->

<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#busqueda").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#indicadores_operacion tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>


<script>

function agregar_indicador(){
	alertify.confirm( 'Desea guardar los indicadores ingresados?', function (e) {
	    if (e) {
	  
	    	inserta_datos();
	    
	    } else {
	    	alertify.error('Cancelado');
	    }
	});   
}






</script>
   
   <div><button type="button" style="margin: 25px;" class="btn btn-danger" onclick="agregar_indicador()">Guardar indicadores</button></div>
   
 <div id="resultado"></div>  
 
