
<link rel="stylesheet" href="plugins/alertify.default.css">
<link rel="stylesheet" href="plugins/alertify.core.css">
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/jquery.table2excel.js"></script>
<script src="plugins/alertify.min.js"></script>





<style>
table {  color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse;}

th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
    border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }

td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
    color: #669;    border-top: 1px solid transparent; }

tr:hover td { background: #d0dafd; color: #339; }


	.select2-container--default .select2-selection--single, .w3-input
	{
		padding: 5px;
	    display: block;
	    border: none;
	    border-bottom: 1px solid #ccc;
	}
	
	.ms-options ms-active
	{
		min-height: 100px; 
		max-height: 100px;
	}
	
	.ms-drop.bottom
	{
		width: 310px;
		
	}

    
#busqueda{

    width: 204px;
}


#buscar{
    margin-left: 30px;
        padding: 7px;
}


#btnExport{
   cursor: pointer;
   line-height: 20px;
    width: 70px;
     margin-right: 130px;
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    padding: 0.5em 1em;
    border: 1px solid #999;
    border-radius: 2px;
    cursor: pointer;
    font-size: 0.88em;
    color: black;
    white-space: nowrap;
    overflow: hidden;
    background-color: #e9e9e9;
    background-image: -webkit-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -moz-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -ms-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: -o-linear-gradient(top, #fff 0%, #e9e9e9 100%);
    background-image: linear-gradient(to bottom, #fff 0%, #e2dcdc 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#e9e9e9');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;

    
}


#excel{
width: 500px;
height: 40px;
}
</style>



<?php
session_start ();
if ($_SESSION ['authenticated'] == 1) {
	include("../../modelo/conexion.php");
    $con=new conexion();
    
	
	//Obtiene el valor del radioboton que determina la manera de buesqueda del indicador
	$busqueda=$_POST['rbtnBusqueda'];
	
	$fecha_inicio=$_POST['fecha_ini'];
	$fecha_fin=$_POST['fecha_fin'];
	

	

	

		
	if ($busqueda=='general'){
		
		$res=$con->conexion->query("select descripcion_servicio,cliente,zona,fecha,ans_acordado,
       indicador_cr,indicador_cl,indicador_general,cumplimiento,justificacion,plan_accion from indicador_operacion where 
       fecha BETWEEN '$fecha_inicio' and '$fecha_fin'");
		
	}else{
		$cliente=$_POST['clientes'];
		
		$res=$con->conexion->query("select a.descripcion_servicio,a.cliente,a.zona,a.fecha,a.ans_acordado,
        a.indicador_cr,a.indicador_cl,a.indicador_general,a.cumplimiento,a.justificacion,a.plan_accion from 
        indicador_operacion a,cliente b where a.cliente=b.cliente and b.id_cliente=$cliente and 
        a.fecha BETWEEN '$fecha_inicio' and '$fecha_fin'");
	}
	

}
?>










 <body>
 
 <div style=" width: 101.5%; height:280px; overflow-y: scroll;">

 


<table id="indicadores_operacion" >  
        <thead>   
          <tr id="tbl_titulo">
           
           <td><b>DESCRIPCIÓN NIVEL SERVICIO </b></td>
           <td><b>CLIENTE</b></td>
           <td><b>ZONA</b></td>
           <td><b>FECHA</b></td>
           <td><b>ANS ACORDADO </b></td>
           <td><b>INDICADOR CR (%)</b></td>
           <td><b>INDICADOR CL (%)</b></td>
           <td><b>INDICADOR GRAL (%)</b></td>
           <td><b>CUMPLIMIENTO (%)</b></td>
           <td><b>JUSTIFICACIÓN</b></td>
           <td><b>PLAN DE ACCIÓN</b></td>
          
          </tr>
        </thead>
        <?php 
        
        while($indi=$res->fetch_assoc()){
        	
        	
        	$nombre_mes = $indi['fecha'];
$mes = explode ('-',$nombre_mes);

//echo $mes[0];  //este valor sera "año"
//echo $mes[1];  //este valor sera "mes"
//echo $mes[2];  //este valor sera "dia"



$m=convertir_num($mes[1]);


//realizamos consulta para tomar el nombre del mes segun el id
$mes_nom=$con->conexion->query("select descripcion from mes where id_mes='$m'");

$nom_mes=$mes_nom->fetch_assoc();


 
?>
               <tbody>   
        	<tr>
        	   <td><?php echo $indi['descripcion_servicio']?></td>
        	   <td><?php echo $indi['cliente']?></td>
        	   <td><?php echo $indi['zona']?></td>
        	   <td><?php echo $nom_mes['descripcion']." ".$mes[0]?></td>
        	   <td><?php echo $indi['ans_acordado']?></td>
        	   <td><?php echo $indi['indicador_cr']."%"?></td>
        	   <td><?php echo $indi['indicador_cl']."%"?></td>
        	   <td><?php echo $indi['indicador_general']."%"?></td>
        	   <td><?php echo $indi['cumplimiento']."%"?></td>
        	   <td><?php echo $indi['justificacion']?></td>
        	   <td><?php echo $indi['plan_accion']?></td>
        	</tr>
        	
        	
        <?php }?>
      
      </tbody>
             
      
      <div id="excel"> 
         
  <button id="btnExport" class="btn btn-danger pull-right">Excel</button>   
     
  <label id="buscar"><b>Buscar:<b></label>
  <input type="text" id='busqueda'>

          </div>   
          
                   
        </table>
              
        
        <br>
        	
 
   </div>
   

   </body>
   
<script>


$("#btnExport").click(function(){
	  $("#indicadores_operacion").table2excel({
	    filename: "Indicadores de Operación"
	  }); 
	});
	 


    /*function exportexcel() {  
        $("#indicadores_operacion").table2excel({  
            name: "Table2Excel",  
            filename: "myFileName",  
            fileext: ".xls"  
        });  
    } */ 

</script>

<?php 
function convertir_num($num){

  

    $cadena = $num;
   
    //$nueva_cadena = ereg_replace("[0]", "", $cadena);
    $resultado = str_replace("0", "", $cadena);

 
    //return $nueva_cadena;
    return $resultado;
}
?>


<!-- script para filtrar en la busqueda de la tabla -->

<script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#busqueda").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#indicadores_operacion tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>





 